module edu.ntnu.idatt2001.trygveam.mappe_2 {
    requires javafx.controls;
    requires javafx.fxml;

    opens edu.ntnu.idatt2001.trygveam.mappe_2 to javafx.fxml;
    exports edu.ntnu.idatt2001.trygveam.mappe_2;
    exports edu.ntnu.idatt2001.trygveam.mappe_2.controllers;
    opens edu.ntnu.idatt2001.trygveam.mappe_2.controllers to javafx.fxml;
    exports edu.ntnu.idatt2001.trygveam.mappe_2.factory;
    opens edu.ntnu.idatt2001.trygveam.mappe_2.factory to javafx.fxml;
    exports edu.ntnu.idatt2001.trygveam.mappe_2.core;
    opens edu.ntnu.idatt2001.trygveam.mappe_2.core to javafx.fxml;
}